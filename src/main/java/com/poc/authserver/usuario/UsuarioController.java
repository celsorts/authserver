package com.poc.authserver.usuario;

import java.security.Principal;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
  @Autowired
  private UsuarioService service;

  @GetMapping
  public Principal validar(Principal principal) {
    return principal;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Usuario criar(@RequestBody @Valid Usuario usuario){
    return service.criar(usuario);
  }
}
