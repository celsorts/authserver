package com.poc.authserver.usuario;

public enum UsuarioRole {
    USER,
    ADMIN;
}