package com.poc.authserver.usuario;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {
  @Id
  private String cpf;
  
  @NotBlank
  private String senha;

  @NotNull
  @Enumerated(EnumType.STRING)
  private UsuarioRole role;
  
  public String getCpf() {
    return cpf;
  }
  
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  
  public String getSenha() {
    return senha;
  }
  
  public void setSenha(String senha) {
    this.senha = senha;
  }

  public UsuarioRole getRole() {
    return role;
  }

  public void setRole(UsuarioRole role) {
    this.role = role;
  }
}
